function formatDate() {
  const d = new Date()

  let day = d.getDate()
  let month = d.toLocaleString('default', { month: 'long' })
  let year = d.getFullYear()
  let hours = d.getHours()
  let minutes = d.getMinutes().toString().padStart(2, '0')

  return `${day} ${month} ${year}, ${hours}:${minutes}`
}

document.addEventListener('DOMContentLoaded', function () {
  const countryListContainer = document.getElementById('country-list')
  const searchInput = document.getElementById('search')

  function fetchCountries() {
    const url = 'https://restcountries.com/v3.1/all'
    fetch(url)
      .then((response) => response.json())
      .then((data) => {
        displayCountries(data)
      })
      .catch((error) => console.error('Error fetching countries:', error))
  }

  function getCurrencyString(currencies) {
    return Object.values(currencies)
      .map((currency) => `${currency.name}`)
      .join(', ')
  }

  function displayCountries(countries) {
    countryListContainer.innerHTML = ''
    countries.forEach((country) => {
      const countryItem = document.createElement('div')
      countryItem.classList.add('country-item')
      countryItem.innerHTML = `
      <div class="image-container"><img class="country-list-flag" src="${
        country.flags.png
      }" alt="${country.name.common} Flag"/></div>
      <div>
        <h2 class="custom-line-height">${country.name.common}</h2>
        <p>Currency: ${getCurrencyString(country.currencies)}</p>
        <p>Current Time & Date: ${formatDate()}</p>
        <button class="show-map" data-url="${
          country.maps.googleMaps
        }">Show Map</button>
        <button class="show-detail" data-cca3="${country.cca3}">Detail</button>
      </div>
      `
      countryListContainer.appendChild(countryItem)
    })

    document.querySelectorAll('.show-map').forEach((button) => {
      button.addEventListener('click', function () {
        showMap(this.dataset.url)
      })
    })

    document.querySelectorAll('.show-detail').forEach((button) => {
      button.addEventListener('click', function () {
        showDetail(this.dataset.cca3)
      })
    })
  }

  function showMap(url) {
    window.open(url, '_blank')
  }

  function showDetail(cca3) {
    window.location.href = `detail.html?country=${cca3}`
  }

  searchInput.addEventListener('input', function () {
    const query = searchInput.value.toLowerCase()
    const countryItems = document.querySelectorAll('.country-item')
    countryItems.forEach((item) => {
      const countryName = item.querySelector('h2').innerText.toLowerCase()
      if (countryName.includes(query)) {
        item.style.display = 'flex'
      } else {
        item.style.display = 'none'
      }
    })
  })

  fetchCountries()
})
