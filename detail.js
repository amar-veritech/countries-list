document.addEventListener('DOMContentLoaded', function () {
  const countryFlag = document.getElementById('country-flag')
  const countryName = document.getElementById('country-name')
  const nativeName = document.getElementById('native-name')
  const capital = document.getElementById('capital')
  const population = document.getElementById('population')
  const region = document.getElementById('region')
  const subregion = document.getElementById('subregion')
  const area = document.getElementById('area')
  const countryCode = document.getElementById('country-code')
  const languages = document.getElementById('languages')
  const currencies = document.getElementById('currencies')
  const timezones = document.getElementById('timezones')
  const neighbourCountriesContainer = document.getElementById(
    'neighbour-countries',
  )

  function getCountryCodeFromUrl() {
    const params = new URLSearchParams(window.location.search)
    return params.get('country')
  }

  function fetchCountryDetail(cca3) {
    const url = `https://restcountries.com/v3.1/alpha/${cca3}`
    fetch(url)
      .then((response) => response.json())
      .then((data) => {
        displayCountryDetail(data[0])
      })
      .catch((error) => console.error('Error fetching country detail:', error))
  }

  function displayCountryDetail(country) {
    countryFlag.src = country.flags.png
    countryName.innerText = country.name.common
    nativeName.innerText = country.name.nativeName
      ? Object.values(country.name.nativeName)[0].common
      : 'N/A'
    capital.innerText = country.capital ? country.capital[0] : 'N/A'
    population.innerText = country.population.toLocaleString()
    region.innerText = country.region
    subregion.innerText = country.subregion
    area.innerText = country.area
    countryCode.innerText = `+${country.idd.root}${
      country.idd.suffixes ? country.idd.suffixes[0] : ''
    }`
    languages.innerText = country.languages
      ? Object.values(country.languages).join(', ')
      : 'N/A'
    currencies.innerText = country.currencies
      ? Object.values(country.currencies)
          .map((curr) => curr.name)
          .join(', ')
      : 'N/A'
    timezones.innerText = country.timezones
      ? country.timezones.join(', ')
      : 'N/A'

    // Fetch and display neighbouring countries
    if (country.borders && country.borders.length > 0) {
      country.borders.forEach((border) => fetchNeighbourCountry(border))
    } else {
      neighbourCountriesContainer.innerHTML =
        '<p>No neighbouring countries found.</p>'
    }
  }

  function fetchNeighbourCountry(neighbourCca3) {
    const url = `https://restcountries.com/v3.1/alpha/${neighbourCca3}`
    fetch(url)
      .then((response) => response.json())
      .then((data) => {
        const neighbourCountry = data[0]
        const neighbourCountryElement = document.createElement('img')
        neighbourCountryElement.src = neighbourCountry.flags.png
        neighbourCountryElement.alt = neighbourCountry.name.common
        neighbourCountryElement.title = neighbourCountry.name.common
        neighbourCountryElement.addEventListener('click', () => {
          window.location.href = `detail.html?country=${neighbourCountry.cca3}`
        })
        neighbourCountriesContainer.appendChild(neighbourCountryElement)
      })
      .catch((error) =>
        console.error('Error fetching neighbour country:', error),
      )
  }

  const countryCodeFromUrl = getCountryCodeFromUrl()
  if (countryCodeFromUrl) {
    fetchCountryDetail(countryCodeFromUrl)
  } else {
    countryDetailContainer.innerHTML =
      '<p>Error: Country code not found in URL.</p>'
  }
})
